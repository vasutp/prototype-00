﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
	private CapsuleCollider collider;
	public string opponentTag = "Enemy";

	public int layerMask = 8;
	public Collider[] hitCollider;


	void Start()
	{
		collider = GetComponent<CapsuleCollider> ();
		layerMask = 1 << layerMask;
	}

	public bool FindEnemy()
	{
		Vector3 p1 = transform.position + collider.center;

		hitCollider = Physics.OverlapSphere (p1, 2, layerMask);
		if ( hitCollider.Length > 0)
		{
			for(int i=0; i<hitCollider.Length; i++)
				if(hitCollider[i].CompareTag(opponentTag))
					return true;
		}

		return false;
	}


	public void DoDamage()
	{
		for (int i = 0; i < hitCollider.Length; i++) 
		{
			if (hitCollider [i] != null)
				if (hitCollider [i].GetComponent<Health> () != null)
					hitCollider [i].GetComponent<Health> ().ReduceHealth ();
		}
	}

	public void DestroyAll()
	{
		Vector3 p1 = transform.position + collider.center;

		Collider[] hitCollider = Physics.OverlapSphere (p1, 8, layerMask);
		if ( hitCollider.Length > 0)
		{
			for(int i=0; i<hitCollider.Length; i++){
				if (hitCollider [i].CompareTag (opponentTag)) 
				{
					if (hitCollider [i].GetComponent<Health> () != null)
						hitCollider [i].GetComponent<Health> ().ReduceHealth (100.0f);
				}
			}
		}
	}
}
