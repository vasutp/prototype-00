﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum eAnimState { Idle, Running, Attack, Skill }

public class AnimState : MonoBehaviour 
{

	public eAnimState animState = eAnimState.Idle;

	public eAnimState CurrentAnimState
	{
		get { return animState; }
		set { animState = value; }
	}


}
