﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour 
{
	public Transform target;
	public float smoothTime = 0.3F;
	public Vector3 distVector = Vector3.zero;



	private Vector3 velocity = Vector3.zero;

	void Update() 
	{
		Vector3 targetPosition = target.TransformPoint(distVector);
		transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);

		transform.LookAt (target);
	}

}
