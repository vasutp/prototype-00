﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackEvent : MonoBehaviour 
{
	private Animator animator;
	private PlayerCollision playerCollision;
	private AnimationClip actionClip;

	public string clipName;
	public float clipTime = 0.11f;


	// Use this for initialization
	void Start () 
	{
		animator = GetComponent<Animator> ();
		AnimationClip[] animClips = animator.runtimeAnimatorController.animationClips;

		playerCollision = GetComponent<PlayerCollision> ();

		for (int i = 0; i < animClips.Length; i++) 
		{
			if (animClips [i].name == clipName)
				actionClip = animClips [i];
		}

		// event
		AnimationEvent evt = new AnimationEvent();
		evt.functionName = "CallActionEvent";
		evt.time = clipTime;

		// set event
		if(actionClip != null)
			actionClip.AddEvent(evt);
	}

	public void CallActionEvent()
	{
		playerCollision.DoDamage ();
	}
}
