﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimatorController : AnimatorController 
{
	private Animator animator;

	// Use this for initialization
	void Start () 
	{
		animator = GetComponent<Animator> ();	
	}
	
	public override void SetAnimation(string name, int value)
	{
		animator.SetInteger (name, value);
	}

	public override void SetAnimation(string name, bool value)
	{
		animator.SetBool (name, value);
	}

	public override void SetIdleAnimation()
	{
		ResetAnimations ();
		SetAnimation ("param_toidle", true);
	}

	public override void SetRunAnimation()
	{
		ResetAnimations ();
		SetAnimation("param_idletorunning", true);
	}

	public override void SetHitAnimation()
	{
		ResetAnimations ();
		SetAnimation("param_idletohit01", true);
	}

	public override void SetDieAnimation()
	{
		ResetAnimations ();
		SetAnimation ("param_idletoko_big", true);
	}

	public override void SetSpecialSkillAnimation()
	{
		ResetAnimations ();
		SetAnimation ("param_idletohit02", true);
	}


	public override void ResetAnimations()
	{
		animator.SetBool("param_idletojump", false);
		animator.SetBool("param_idletowalk", false);
		animator.SetBool("param_idletorunning", false);
		animator.SetBool("param_idletohit01", false);
		animator.SetBool("param_idletohit02", false);
		animator.SetBool("param_idletohit03", false);
		animator.SetBool("param_idletoko_big", false);
		animator.SetBool("param_idletodamage", false);
		animator.SetBool("param_idletodefault", false);
		animator.SetBool("param_idletowinpose", false);
		animator.SetBool("param_toidle", false);
	}
}
