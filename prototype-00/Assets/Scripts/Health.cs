﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour 
{
	public float currentHealth = 100.0f;
	public float damageFromEnemy = 10.0f;

	private AnimatorController animatorController;

	void Start()
	{
		animatorController = GetComponent<AnimatorController> ();
	}

	public float CurrentHealth
	{
		get { return currentHealth; }
		set { 
			currentHealth = value; 
			if(currentHealth <= 0)
				currentHealth = 0.0f;
		}
	}

	public void ReduceHealth()
	{
		CurrentHealth -= damageFromEnemy;
		if (IsDie ()) {
			if(animatorController != null)
				animatorController.SetDieAnimation ();
		}
	}

	public void ReduceHealth(float val)
	{
		CurrentHealth -= val;

		if (IsDie ()) {
			if(animatorController != null)
				animatorController.SetDieAnimation ();
		}
	}

	public bool IsDie()
	{
		if (currentHealth <= 0)
			return true;
		return false;
	}

	public void Reset()
	{
		CurrentHealth = 100.0f;
	}
}
