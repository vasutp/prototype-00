﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InGameHUDScreen : MonoBehaviour 
{
	public float skillButtonActiveTime = 10.0f;

	public Button skillButton; 
	public static Action OnClickSkillAction;

	public GameObject cam4_settings;

	public GameObject directinalLightObj;

	void Awake()
	{
		if (SceneManager.GetActiveScene ().name == "Main_4")
			cam4_settings.gameObject.SetActive (true);
		else
			cam4_settings.gameObject.SetActive (false);

		directinalLightObj = GameObject.Find ("Directional Light");

	}

	public void OnClickSkillButton()
	{
		if (OnClickSkillAction != null)
			OnClickSkillAction ();

		skillButton.gameObject.SetActive (false);
		StartCoroutine ("DeactivateSkillButton");
	}

	private IEnumerator DeactivateSkillButton()
	{
		yield return new WaitForSeconds (skillButtonActiveTime);
		skillButton.gameObject.SetActive (true);
	}

	public void OnClickCamera1()
	{
		SceneManager.LoadScene ("Main");
	}

	public void OnClickCamera2()
	{
		SceneManager.LoadScene ("Main_2");
	}

	public void OnClickCamera3()
	{
		SceneManager.LoadScene ("Main_4");
	}

	public void OnClickDirectionalLight()
	{
		Light dirLight = null;
		if (directinalLightObj != null)
			dirLight = directinalLightObj.GetComponent<Light> ();

		if (dirLight != null) {
			if (!dirLight.enabled)
				dirLight.enabled = true;
			else
				dirLight.enabled = false;
		}
			
	}

	public void OnClickPointLight()
	{
		Light pointLight = null;
		GameObject player = GameObject.FindWithTag ("Player");
		pointLight = player.GetComponentInChildren<Light> ();

		if (pointLight != null) {
			if (!pointLight.enabled)
				pointLight.enabled = true;
			else
				pointLight.enabled = false;
		}

	}
}
