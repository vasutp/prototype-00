﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour 
{
	private AnimatorController animatorController;
	private AnimState animState;

	void Start()
	{
		animatorController = GetComponent<AnimatorController> ();
		animState = GetComponent<AnimState> ();

		InGameHUDScreen.OnClickSkillAction += OnClickSkillAction;
	}

	void OnDisable()
	{
		InGameHUDScreen.OnClickSkillAction -= OnClickSkillAction;
	}

	private void OnClickSkillAction()
	{
		animState.CurrentAnimState = eAnimState.Skill;
		animatorController.SetSpecialSkillAnimation ();
	}
}
