﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimatorController : AnimatorController 
{
	private Animator animator;

	void Start()
	{
		animator = GetComponent<Animator> ();
	}

	public override void SetAnimation(string name, int value)
	{
		animator.SetInteger (name, value);
	}

	public override void SetAnimation(string name, bool value)
	{
		animator.SetBool (name, value);
	}

	public override void SetIdleAnimation()
	{
		ResetAnimations ();
		//SetAnimation ();
	}

	public override void SetRunAnimation()
	{
		ResetAnimations ();
		SetAnimation("idle0ToRun", true);
	}

	public override void SetHitAnimation()
	{
		ResetAnimations ();
		SetAnimation ("idle0ToAttack0", true);
	}

	public override void SetDieAnimation()
	{
		ResetAnimations ();
		SetAnimation ("idle0ToDeath", true);
	}

	public override void ResetAnimations()
	{
		animator.SetBool("idle0ToIdle1", false);
		animator.SetBool("idle0ToWalk", false);
		animator.SetBool("idle0ToRun", false);
		animator.SetBool("idle0ToWound", false);
		animator.SetBool("idle0ToSkill0", false);
		animator.SetBool("idle0ToAttack1", false);
		animator.SetBool("idle0ToAttack0", false);
		animator.SetBool("idle0ToDeath", false);
	}
}
