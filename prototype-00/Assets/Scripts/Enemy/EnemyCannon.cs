﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCannon : MonoBehaviour {
	public GameObject player;
	public GameObject pool;
	public GameObject enemyBulletPool;
	public Transform bulletSpawn;
	public bool homing = false;
	public float shotTimeout = 0;
	public float bulletSpeed = 100.0f;
	public int health = 10;

	void Start() {
		player = GameObject.FindGameObjectWithTag ("Player");
		pool = GameObject.FindGameObjectWithTag ("EnemyCannonBulletPool");
		if (pool == null) {
			Instantiate (enemyBulletPool); 
			pool = GameObject.FindGameObjectWithTag ("EnemyCannonBulletPool");
		}
	}

	void Update () {
		if(homing)
		transform.LookAt(player.transform);
		shotTimeout += Time.deltaTime;
		if(shotTimeout > 3)
		{ 
			GameObject obj = pool.GetComponent<ObjectPoolingScript> ().GetPooledObject ();
			obj.transform.position = bulletSpawn.position;
			obj.transform.rotation = bulletSpawn.rotation;
			obj.GetComponent<Rigidbody> ().velocity = bulletSpawn.transform.forward * bulletSpeed;
			obj.SetActive (true);
			shotTimeout = 0;
		}
		if (health == 0) {
			
			Destroy (gameObject);
		} 
	}

	void OnTriggerEnter (Collider c) {
		if (c.CompareTag ("PlayerBullet")) {
			Debug.Log ("ffff");
			health = health - 5;
		}
	}
}
