﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {

	public float lifetime = 0.3f;
	public float time;

	void Update () {
		time += Time.deltaTime;
		if (time > lifetime) {
			this.gameObject.SetActive (false);
			time = 0;
		}
	}

	void OnTriggerEnter (Collider c) {
		if (c.CompareTag ("Player")) {
			c.GetComponent<Health> ().ReduceHealth();
			GameObject cam = GameObject.Find("Main Camera");
			cam.GetComponent<CameraShake> ().shakeDuration = 0.01f;
			cam.GetComponent<CameraShake> ().enabled = true;
			this.gameObject.SetActive (false);
		}
	}

}
