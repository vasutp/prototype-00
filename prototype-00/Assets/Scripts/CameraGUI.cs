﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraGUI : MonoBehaviour 
{
	public FollowCamera followCamera;

	void Awake()
	{
		followCamera = GameObject.Find ("Main Camera").GetComponent<FollowCamera> ();
	}

	public void SetCamX(float val)
	{
		followCamera.distVector.x = val * 10.0f;
	}

	public void SetCamY(float val)
	{
		followCamera.distVector.y = val * 10.0f;
	}

	public void SetCamZ(float val)
	{
		followCamera.distVector.z = val * 10.0f;
	}
}
