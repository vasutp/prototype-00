﻿

using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FpsCounter : MonoBehaviour
{
	void Start ()
	{
		guiPos = new Rect (0, 0, 100, 100);
		guiStyle = new GUIStyle ();
		guiStyle.fontSize = 50;
		guiStyle.normal.textColor = Color.white;
		//m_Fps_Text.gameObject.SetActive (false);
	}



	//public Text m_Fps_Text;

	int frameCount = 0;
	float dt = 0.0f;
	float fps = 0.0f;
	float updateRate = 1f;  // 4 updates per sec.
	
	Rect guiPos;
	GUIStyle guiStyle;


	void OnGUI(){
		frameCount++;
		dt += Time.deltaTime;
		if (dt > 1f / updateRate) {
			fps = frameCount / dt;
			frameCount = 0;
			dt -= 1f / updateRate;
			//m_Fps_Text.text = fps.ToString ();
		}
		GUI.Label (guiPos, "FPS: " + ((int)fps).ToString (), guiStyle);
	}


	public void OnDestroy ()
	{
		//CommandHandlers.UnregisterCommandHandlers (this);
		
	}
}