﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorController : MonoBehaviour 
{
	public eAnimState animState;

	public virtual void SetAnimation (string name, int value){}
	public virtual void SetAnimation (string name, bool value){}
	public virtual void SetIdleAnimation(){}
	public virtual void SetRunAnimation (){}
	public virtual void SetHitAnimation(){}
	public virtual void SetDieAnimation() {}
	public virtual void SetSpecialSkillAnimation(){}
	public virtual void ResetAnimations(){}
}
