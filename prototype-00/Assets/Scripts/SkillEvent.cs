﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillEvent : MonoBehaviour 
{
	private Animator animator;
	private PlayerCollision playerCollision;
	private AnimationClip actionClip;
	private AnimState animState;
	private AnimatorController animatorController;


	public string clipName;
	public float clipTime = 0.11f;
	public GameObject particleEffect;

	// Use this for initialization
	void Start () 
	{
		animator = GetComponent<Animator> ();
		AnimationClip[] animClips = animator.runtimeAnimatorController.animationClips;
		animState = GetComponent<AnimState> ();
		animatorController = GetComponent<AnimatorController> ();

		playerCollision = GetComponent<PlayerCollision> ();

		for (int i = 0; i < animClips.Length; i++) 
		{
			if (animClips [i].name == clipName)
				actionClip = animClips [i];
		}

		// event
		AnimationEvent evt = new AnimationEvent();
		evt.functionName = "CallSkillEvent";
		evt.time = clipTime;

		// set event
		if(actionClip != null)
			actionClip.AddEvent(evt);
	}

	public void CallSkillEvent()
	{
		// perform particle effect
		GameObject particle = GameObject.Instantiate(particleEffect, transform.position + new Vector3(0,0.1f,0) , Quaternion.identity) as GameObject;
		Destroy (particle, 1.0f);

		if (animState != null)
			animState.CurrentAnimState = eAnimState.Idle;

		if(animatorController != null)
			animatorController.ResetAnimations ();

		if (playerCollision != null)
			playerCollision.DestroyAll ();
	}
}
