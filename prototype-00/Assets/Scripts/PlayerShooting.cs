﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour {

	public GameObject pool;
	public GameObject playerBulletPool;
	public Transform bulletSpawn;
	public float bulletRegenCd;
	public float bulletRegenLimit;
	public int clipSize;
	public bool canShoot;
	public float bulletSpeed;

	// Use this for initialization
	void Start () {
		pool = GameObject.FindGameObjectWithTag ("PlayerBulletPool");
		if (pool == null) {
			Instantiate (playerBulletPool); 
			pool = GameObject.FindGameObjectWithTag ("PlayerBulletPool");
		}
	}
	
	// Update is called once per frame
	void Update () {
		/*if (clipSize > 0) { 
			GameObject obj = pool.GetComponent<ObjectPoolingScript> ().GetPooledObject ();
			obj.transform.position = bulletSpawn.position;
			obj.transform.rotation = bulletSpawn.rotation;
			obj.GetComponent<Rigidbody> ().velocity = bulletSpawn.transform.forward * bulletSpeed;
			obj.SetActive (true);
			clipSize--;
		} */

		if (Input.GetMouseButtonDown (0) && clipSize>0) {
			var mousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			//mousePosition.y = 0;
			//mousePosition.z = 0;
			mousePosition.x = 0;
			Vector3 directionalVector = (transform.position - mousePosition).normalized;
		
			Debug.Log(directionalVector);
			GameObject obj = pool.GetComponent<ObjectPoolingScript> ().GetPooledObject ();
			obj.transform.position = bulletSpawn.position;

			//obj.transform.LookAt (mousePosition);
			obj.GetComponent<Rigidbody> ().velocity = directionalVector * bulletSpeed;
			obj.SetActive (true);
			Debug.Log ("use it use it use it");
			clipSize--;
		}

		if (Input.touchCount > 0 &&  Input.GetTouch(0).phase == TouchPhase.Began && clipSize > 0) {
			var fingerPos = Input.GetTouch(0).position;
			var touchPosition = Camera.main.ScreenToWorldPoint (fingerPos);
			//mousePosition.y = 0;
			//mousePosition.z = 0;
			touchPosition.x = 0;
			Vector3 directionalVector = (transform.position - touchPosition).normalized;
			Debug.Log(directionalVector);
			GameObject obj = pool.GetComponent<ObjectPoolingScript> ().GetPooledObject ();
			obj.transform.position = bulletSpawn.position;

			//obj.transform.LookAt (mousePosition);
			obj.GetComponent<Rigidbody> ().velocity = directionalVector * bulletSpeed;
			obj.SetActive (true);
			Debug.Log ("use it use it use it");
			clipSize--;
		}
	

		if(clipSize == 0) {
			bulletRegenCd += Time.deltaTime;
			if (bulletRegenCd >= bulletRegenLimit) {
				clipSize = 100;
			}
		}
	}
}
