﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationTowardsPlayer : MonoBehaviour 
{
	private GameObject currentTarget;
	private PlayerCollision playerCollision; 

	public float slerpRotationSpeed = 2.0f;

	private void Start()
	{
		currentTarget = GameObject.FindWithTag ("Player");
		playerCollision = GetComponent<PlayerCollision> ();
	}

	void Update()
	{
		bool foundEnemy = playerCollision.FindEnemy ();
		if (foundEnemy)
			Rotate ();
	}


	private void Rotate()
	{
		Quaternion targetRotation = Quaternion.LookRotation(currentTarget.transform.position - transform.position);
		transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * slerpRotationSpeed);
	}
}
